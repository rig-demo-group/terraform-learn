provider "aws" {
    region = "eu-central-1"
    access_key = ""
    secret_key = ""
}

# credentials can be set in two ways
# 1 - as env variables through export AWS_SECRET_ACCESS_KEY= KEY VALUE , same as for CLI
# to have them globally configured, they need to be set in ~./aws/credentials directory through aws configure 
# when in need, check the docs, it's good

#global variable can be set as export TF_VAR_%ANY NAME%="" e.g. TF_VAR_avail_zone="eu-central-1"

# best practice is not to hardcode the access keys in config file directly
# main.tf - logic of the infrastructure
# providers.tf - list of used providers

# first argument - resource (check the document), second - the name of the resource we decide to give it

variable avail_zone {}
#this global variable can be referenced in code e.g. availability_zone = var.avail_zone

variable "cidr_blocks" {
    description = "var vpc ciderblock"
    type = list(object({
        cidr_block = string
        name = string
    }))
}

variable "environment" {
    description = "deployment environment"
}

#referencing 2nd element from the list of strings
#syntax var.cidr_blocksp[0].cidr_block allows us to access first element in the list which is cidr_block (since it is a string)

resource "aws_vpc" "development-vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block 
    tags = {
        Name: var.environment[0].name
    }
}

#tag Name is reserved for naming purposes. attention: first letter is capital

#3 ways to define a variable:
#1-upon apply we get a prompt for a variable
#2-using command line argument upon apply ( terraform apply -var "subnet_cidr_block=10.0.0.1/24")
#3-in a var file file / in config

#default makes variable optional
# type - enforce a type of value


variable "subnet_cidr_block" {
    description = "subnet cidr block"
    default = "10.0.10.0/24"
    type = string
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = "eu-central-1a"
    tags = {
      Name: "subnet-1-dev"
    }
}
#we can reference an object that is not created yet, thus upon creation, vpc_id can be checked there

# we can query aws for data to be used in terraform

data "aws_vpc" "existing_vpc" {
    default = true
}

resource "aws_subnet" "dev-subnet-2" {
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = "eu-central-1a"
    tags = {
      Name: "subnet-2-default"
    }
}


# we can remove a resource in two ways: either deleting it from configuration or using terraform destroy -target aws_subnet.dev-subnet-2
# best option is to use terraform apply + deleting from main.tf 

#terraform plan - check the actual state vs config 
#terraform apply -auto-approve - execute without asking
# terraform destroy (with no arguments) - deletes everything 
# terraform state - commands to access tfstate 


output "dev-vpc-id" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}

#output allows us to see in the terminal the information we require about resources we've created. saves time looking for it elsewhere